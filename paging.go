// main.go
package main

import (
	"context"
	"fmt"
	"net/http"
	"strconv"
)

type Paging struct {
	Previous string `json:"previous"`
	Next     string `json:"next"`
	Last     bool   `json:"last"`
	Page     int    `json:"page"`
}

type paginateHelper struct {
	paging   Paging
	indexMin int
	indexMax int
}

type pageLimit struct {
	page  int
	limit int
}

type cachekey string

const pageKey cachekey = "pageKey"
const limitKey cachekey = "limitKey"

func pagingMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// Do stuff here

		page_ := r.URL.Query().Get("page")
		limit_ := r.URL.Query().Get("limit")
		page := 1
		limit := 10
		var err error
		if page_ != "" {
			page, err = strconv.Atoi(page_)
			if err != nil {
				http.Error(w, err.Error(), http.StatusBadRequest)
				return
			}
			if page < 1 {
				page = 1
			}
		}

		if limit_ != "" {
			limit, err = strconv.Atoi(limit_)
			if err != nil {
				http.Error(w, err.Error(), http.StatusBadRequest)
				return
			}
		}

		ctx := context.WithValue(r.Context(), pageKey, page)
		ctx = context.WithValue(ctx, limitKey, limit)

		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

func getPageLimit(con context.Context) pageLimit {
	page := con.Value(pageKey).(int)
	limit := con.Value(limitKey).(int)
	return pageLimit{page: page, limit: limit}

}
func viewSlice(length int, pl pageLimit, path string) paginateHelper {

	limit := pl.limit
	page := pl.page

	imin := min(length, limit*(page-1))
	imax := min(limit*page, length)
	page = min((length+limit-1)/limit, page)
	var last bool
	if imax == length {
		last = true
	} else {
		last = false
	}
	previous := fmt.Sprintf("%s?page=%d&limit=%d", path, page, limit)
	next := fmt.Sprintf("%s?page=%d&limit=%d", path, page+1, limit)

	if last {
		next = ""
	}
	if page == 1 {
		previous = ""
	}

	return paginateHelper{paging: Paging{previous, next, last, page}, indexMin: imin, indexMax: imax}
}
