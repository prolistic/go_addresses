package main

import (
	"database/sql"
	"encoding/gob"
	"fmt"
	"log"
	"os"
	"sort"
	"strings"
	"unicode"

	"golang.org/x/text/transform"
	"golang.org/x/text/unicode/norm"

	"github.com/agnivade/levenshtein"
	_ "github.com/denisenkom/go-mssqldb"
)

func abs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}

func min(x, y int) int {
	if x < y {
		return x
	}
	return y
}


func isMn(r rune) bool {
	return unicode.Is(unicode.Mn, r) // Mn: nonspacing marks
}

func normalize(s string, t transform.Transformer) string {

	result, _, _ := transform.String(t, s)
	return result
}

type Address struct {
	ID        int    `json:"id"`
	IDParent  int    `json:"idParent"`
	Level     int    `json:"level"`
	Name      string `json:"name"`
	LastLevel bool   `json:"lastLevel"`
}

type Alias struct {
	ID        int    `json:"id"`
	IDAddress int    `json:"idAdress"`
	Name      string `json:"level"`
	Code      int    `json:"code"`
}

type AddressDist struct {
	Address  `json:"address"`
	Distance int `json:"distance"`
}

type AliasDist struct {
	Alias    `json:"alias"`
	Distance int `json:"distance"`
}
type AddressChilds map[int][]int

//map int -> addressDb
type AddressMap map[int]Address
type AliasMap map[int]Alias

type Childs map[int][]int

type Index []int

type AddressesDB struct {
	M                AddressMap
	Childs           Childs
	Alias            AliasMap
	ILevelName       Index
	IParentName      Index
	ILevelNameAlias  Index
	IParentNameAlias Index
	IAddressAlias    Index
	t                transform.Transformer
}
type AddressLevel struct {
	Level int    `json:"level" example:"1"`
	Name  string `json:"name" example:"Netherlands"`
}

type AddressTree []AddressLevel

func makeAddressesDB(conf DatabaseConfig) *AddressesDB {
	aDB := new(AddressesDB)
	aDB.M = makeAddressMapFormSql(conf)
	aDB.Alias = makeAliassesMapFormSql(conf)
	aDB.makeIndexLevel()
	aDB.makeIndexParent()
	aDB.makeChilds()
	aDB.makeIndexLevelAlias()
	aDB.makeIndexParentAlias()
	aDB.makeIndexAddressAlias()
	aDB.t = transform.Chain(norm.NFD, transform.RemoveFunc(isMn), norm.NFC)
	return aDB
}

func (aDB AddressesDB) lessByILevelName(i, j int) bool {

	mi := aDB.M[aDB.ILevelName[i]]
	mj := aDB.M[aDB.ILevelName[j]]

	if mi.Level < mj.Level {
		return true
	} else if mi.Level > mj.Level {
		return false
	} else {
		return mi.Name < mj.Name
	}
}

func (aDB AddressesDB) lessByILevelNameAlias(i, j int) bool {

	ai := aDB.Alias[aDB.ILevelNameAlias[i]]
	aj := aDB.Alias[aDB.ILevelNameAlias[j]]
	leveli := aDB.M[ai.IDAddress].Level
	levelj := aDB.M[aj.IDAddress].Level

	if leveli < levelj {
		return true
	} else if leveli > levelj {
		return false
	} else {
		return ai.Name < aj.Name
	}
}

func (aDB AddressesDB) lessByIParentName(i, j int) bool {
	idxi := aDB.IParentName[i]
	idxj := aDB.IParentName[j]
	if aDB.M[idxi].IDParent < aDB.M[idxj].IDParent {
		return true
	} else if aDB.M[idxi].IDParent > aDB.M[idxj].IDParent {
		return false
	} else {
		return aDB.M[idxi].Name < aDB.M[idxj].Name
	}
}

func (aDB AddressesDB) lessByIParentNameAlias(i, j int) bool {
	ai := aDB.Alias[aDB.IParentNameAlias[i]]
	aj := aDB.Alias[aDB.IParentNameAlias[j]]
	parenti := aDB.M[ai.IDAddress].IDParent
	parentj := aDB.M[aj.IDAddress].IDParent
	if parenti < parentj {
		return true
	} else if parenti > parentj {
		return false
	} else {
		return ai.Name < aj.Name
	}
}

func (aDB AddressesDB) lessByIAddressAlias(i, j int) bool {
	ai := aDB.Alias[aDB.IAddressAlias[i]]
	aj := aDB.Alias[aDB.IAddressAlias[j]]
	return ai.IDAddress < aj.IDAddress
}

func (aDB *AddressesDB) makeIndexLevel() {

	aDB.ILevelName = make(Index, 0, len(aDB.M))

	for _, v := range aDB.M {
		aDB.ILevelName = append(aDB.ILevelName, v.ID)
	}

	sort.Slice(aDB.ILevelName, aDB.lessByILevelName)

}

func (aDB *AddressesDB) makeIndexParent() {
	aDB.IParentName = make(Index, 0, len(aDB.M))

	for _, v := range aDB.M {
		aDB.IParentName = append(aDB.IParentName, v.ID)
	}

	sort.Slice(aDB.IParentName, aDB.lessByIParentName)
}

func (aDB *AddressesDB) makeIndexLevelAlias() {
	aDB.ILevelNameAlias = make(Index, 0, len(aDB.Alias))

	for _, v := range aDB.Alias {
		aDB.ILevelNameAlias = append(aDB.ILevelNameAlias, v.ID)
	}

	sort.Slice(aDB.ILevelNameAlias, aDB.lessByILevelNameAlias)
}

func (aDB *AddressesDB) makeIndexParentAlias() {
	aDB.IParentNameAlias = make(Index, 0, len(aDB.Alias))

	for _, v := range aDB.Alias {
		aDB.IParentNameAlias = append(aDB.IParentNameAlias, v.ID)
	}

	sort.Slice(aDB.IParentNameAlias, aDB.lessByIParentNameAlias)
}

func (aDB *AddressesDB) makeIndexAddressAlias() {
	aDB.IAddressAlias = make(Index, 0, len(aDB.Alias))

	for _, v := range aDB.Alias {
		aDB.IAddressAlias = append(aDB.IAddressAlias, v.ID)
	}

	sort.Slice(aDB.IAddressAlias, aDB.lessByIAddressAlias)
}

func (aDB AddressesDB) completeFromLevel(level int, partName string) []Address {
	partName = normalize(partName, aDB.t)
	partName = strings.ToUpper(partName)
	idx_low := sort.Search(len(aDB.ILevelName), func(i int) bool {
		if aDB.M[aDB.ILevelName[i]].Level < level {
			return false
		} else if aDB.M[aDB.ILevelName[i]].Level > level {
			return true
		} else {
			n := min(len(partName), len(aDB.M[aDB.ILevelName[i]].Name))
			return aDB.M[aDB.ILevelName[i]].Name[:n] >= partName
		}

	})

	idx_high := sort.Search(len(aDB.ILevelName), func(i int) bool {
		if aDB.M[aDB.ILevelName[i]].Level < level {
			return false
		} else if aDB.M[aDB.ILevelName[i]].Level > level {
			return true
		} else {

			n := min(len(partName), len(aDB.M[aDB.ILevelName[i]].Name))
			return aDB.M[aDB.ILevelName[i]].Name[:n] > partName
		}
	})

	address := make([]Address, 0)
	for i := idx_low; i < idx_high; i++ {
		address = append(address, aDB.M[aDB.ILevelName[i]])
	}
	return address

}

func (aDB AddressesDB) completeFromParent(idParent int, partName string) []Address {

	partName = normalize(partName, aDB.t)
	partName = strings.ToUpper(partName)
	idx_low := sort.Search(len(aDB.IParentName), func(i int) bool {
		if aDB.M[aDB.IParentName[i]].IDParent < idParent {
			return false
		} else if aDB.M[aDB.IParentName[i]].IDParent > idParent {
			return true
		} else {

			n := min(len(partName), len(aDB.M[aDB.ILevelName[i]].Name))
			return aDB.M[aDB.IParentName[i]].Name[:n] >= partName
		}

	})

	idx_high := sort.Search(len(aDB.IParentName), func(i int) bool {
		if aDB.M[aDB.IParentName[i]].IDParent < idParent {
			return false
		} else if aDB.M[aDB.IParentName[i]].IDParent > idParent {
			return true
		} else {

			n := min(len(partName), len(aDB.M[aDB.ILevelName[i]].Name))
			return aDB.M[aDB.IParentName[i]].Name[:n] > partName
		}
	})

	address := make([]Address, 0)
	for i := idx_low; i < idx_high; i++ {
		address = append(address, aDB.M[aDB.IParentName[i]])
	}
	return address
}

//load data from sql
func makeAddressMapFormSql(conf DatabaseConfig) AddressMap {
	t0 := transform.Chain(norm.NFD, transform.RemoveFunc(isMn), norm.NFC)
	connectionstr := fmt.Sprintf("server=%s;DATABASE=%s;UID=%s;password=%s", conf.Server, conf.DB, conf.User, conf.Password)
	//condb, errdb := sql.Open("sqlserver", "server=GEMINI\\sqlexpress;DATABASE=ReceMailPin;UID=sa;password=cesabe")
	condb, errdb := sql.Open("sqlserver", connectionstr)

	if errdb != nil {
		fmt.Println(" Error open db:", errdb.Error())
	}
	nrow := 0
	rows, err := condb.Query("select count(*) from address")
	if err != nil {
		log.Fatal(err)
	}
	for rows.Next() {
		err := rows.Scan(&nrow)
		if err != nil {
			log.Fatal(err)
		}
	}
	m := make(AddressMap, nrow)
	rows, err = condb.Query("select * from address")
	if err != nil {
		log.Fatal(err)
	}
	address := Address{}
	var _IDParent sql.NullInt32
	for rows.Next() {
		err := rows.Scan(&address.ID, &_IDParent, &address.Level, &address.Name, &address.LastLevel)
		if err != nil {
			log.Fatal(err)
		}
		address.Name = normalize(address.Name, t0)
		address.IDParent = int(_IDParent.Int32)
		m[address.ID] = address
	}
	defer condb.Close()
	return m
}

func makeAliassesMapFormSql(conf DatabaseConfig) AliasMap {
	t0 := transform.Chain(norm.NFD, transform.RemoveFunc(isMn), norm.NFC)

	// condb, errdb := sql.Open("sqlserver", "server=GEMINI\\sqlexpress;DATABASE=ReceMailPin;UID=sa;password=cesabe")
	//condb, errdb := sql.Open("sqlserver", "server=GEMINI\\sqlexpress;DATABASE=ReceMailCycloon01;UID=sa;password=cesabe")
	connectionstr := fmt.Sprintf("server=%s;DATABASE=%s;UID=%s;password=%s", conf.Server, conf.DB, conf.User, conf.Password)
	//condb, errdb := sql.Open("sqlserver", "server=GEMINI\\sqlexpress;DATABASE=ReceMailPin;UID=sa;password=cesabe")
	condb, errdb := sql.Open("sqlserver", connectionstr)
	if errdb != nil {
		fmt.Println(" Error open db:", errdb.Error())
	}
	nrow := 0
	rows, err := condb.Query("select count(*) from AddressAlias")
	if err != nil {
		log.Fatal(err)
	}
	for rows.Next() {
		err := rows.Scan(&nrow)
		if err != nil {
			log.Fatal(err)
		}
	}
	m := make(AliasMap, nrow)
	rows, err = condb.Query("SELECT Id, IdAddress, Name, Code FROM AddressAlias")
	if err != nil {
		log.Fatal(err)
	}
	alias := Alias{}
	var _code sql.NullInt32
	for rows.Next() {
		err := rows.Scan(&alias.ID, &alias.IDAddress, &alias.Name, &_code)
		if err != nil {
			log.Fatal(err)
		}
		alias.Name = normalize(alias.Name, t0)
		alias.Code = int(_code.Int32)
		m[alias.ID] = alias
	}
	defer condb.Close()
	return m
}

func (aDB AddressesDB) serialize() {
	// create a file
	dataFile, err := os.Create("addressesDB.gob")

	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	// serialize the data
	dataEncoder := gob.NewEncoder(dataFile)
	dataEncoder.Encode(aDB)

	dataFile.Close()

}

// read address.gob
func loadAddressesDB() *AddressesDB {
	aDB := new(AddressesDB)
	// create a file
	dataFile, err := os.Open("addressesDB.gob")

	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	// serialize the data
	dataDecoder := gob.NewDecoder(dataFile)
	err = dataDecoder.Decode(aDB)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	dataFile.Close()
	fmt.Printf("Number of entries = %v \n", len(aDB.M))
	aDB.t = transform.Chain(norm.NFD, transform.RemoveFunc(isMn), norm.NFC)
	return aDB
}

func (aDb *AddressesDB) makeChilds() {
	addressChilds := make(Childs)

	for indexChild, address := range aDb.M {
		id := address.IDParent
		_, ok := addressChilds[id]
		if ok {
			addressChilds[id] = append(addressChilds[id], indexChild)
		} else {
			addressChilds[id] = make([]int, 0)
		}
	}
	aDb.Childs = addressChilds
}

func (m AddressMap) getParent(id int) int {
	return m[id].IDParent
}

func (aDb *AddressesDB) getChild(id int) []int {
	return aDb.Childs[id]
}

func (aDb *AddressesDB) getParent(id int) int {
	return aDb.M.getParent(id)
}

func (m AddressMap) getAddressTreeFromId(id int) AddressTree {
	// addressTree := make(map[int]string)
	addressTree := make(AddressTree, 0)
	a, ok := m[id]
	if !ok {
		panic("wrong id")
	}

	_id := a.ID

	for _id > 0 {
		al := AddressLevel{m[_id].Level, m[_id].Name}
		addressTree = append(addressTree, al)
		// addressTree[m[_id].Level] = m[_id].Name
		_id = m[_id].IDParent
	}
	return addressTree
}

func (aDb *AddressesDB) searchFuzzyFromParent(id int, name string, maxDist int) []AddressDist {
	partName := normalize(name, aDb.t)
	partName = strings.ToUpper(partName)
	addresses := make([]AddressDist, 0)
	for _, a := range aDb.Childs[id] {
		if abs(len(aDb.M[a].Name)-len(partName)) > maxDist {
			continue
		}
		distance := levenshtein.ComputeDistance(aDb.M[a].Name, partName)
		if distance <= maxDist {
			addresses = append(addresses, AddressDist{Address: aDb.M[a], Distance: distance})
		}
	}
	sort.Slice(addresses, func(i, j int) bool { return addresses[i].Distance < addresses[j].Distance })
	return addresses
}

func (aDb *AddressesDB) searchFuzzyFromLevel(level int, name string, maxDist int) []AddressDist {
	partName := normalize(name, aDb.t)
	partName = strings.ToUpper(partName)
	addresses := make([]AddressDist, 0)

	idx_low := sort.Search(len(aDB.ILevelName), func(i int) bool {
		return aDB.M[aDB.ILevelName[i]].Level >= level
	})

	idx_high := sort.Search(len(aDB.ILevelName), func(i int) bool {
		return aDB.M[aDB.ILevelName[i]].Level > level
	})

	for i := idx_low; i < idx_high; i++ {
		a := aDb.M[aDB.ILevelName[i]]
		distance := levenshtein.ComputeDistance(a.Name, partName)
		if distance <= maxDist {
			addresses = append(addresses, AddressDist{Address: a, Distance: distance})
		}

	}

	sort.Slice(addresses, func(i, j int) bool { return addresses[i].Distance < addresses[j].Distance })
	return addresses
}

func (aDb *AddressesDB) searchFuzzyFromParentAlias(id int, name string, maxDist int) []AliasDist {
	partName := normalize(name, aDb.t)
	partName = strings.ToUpper(partName)
	aliases := make([]AliasDist, 0)

	idx_low := sort.Search(len(aDB.IParentNameAlias), func(i int) bool {
		return aDB.M[aDB.Alias[aDB.IParentNameAlias[i]].IDAddress].IDParent >= id
	})

	idx_high := sort.Search(len(aDB.IParentNameAlias), func(i int) bool {
		return aDB.M[aDB.Alias[aDB.IParentNameAlias[i]].IDAddress].IDParent > id
	})

	for i := idx_low; i < idx_high; i++ {
		a := aDB.Alias[aDB.IParentNameAlias[i]]
		if abs(len(a.Name)-len(partName)) > maxDist {
			continue
		}
		distance := levenshtein.ComputeDistance(a.Name, partName)
		if distance <= maxDist {
			aliases = append(aliases, AliasDist{Alias: a, Distance: distance})
		}
	}

	sort.Slice(aliases, func(i, j int) bool { return aliases[i].Distance < aliases[j].Distance })
	return aliases
}

func (aDb *AddressesDB) searchFuzzyFromLevelAlias(level int, name string, maxDist int) []AliasDist {
	partName := normalize(name, aDb.t)
	partName = strings.ToUpper(partName)
	aliases := make([]AliasDist, 0)

	idx_low := sort.Search(len(aDB.ILevelNameAlias), func(i int) bool {
		return aDB.M[aDB.Alias[aDB.ILevelNameAlias[i]].IDAddress].Level >= level
	})

	idx_high := sort.Search(len(aDB.ILevelNameAlias), func(i int) bool {
		return aDB.M[aDB.Alias[aDB.ILevelNameAlias[i]].IDAddress].Level > level
	})

	for i := idx_low; i < idx_high; i++ {
		a := aDB.Alias[aDB.ILevelNameAlias[i]]
		if abs(len(a.Name)-len(partName)) > maxDist {
			continue
		}
		distance := levenshtein.ComputeDistance(a.Name, partName)
		if distance <= maxDist {
			aliases = append(aliases, AliasDist{Alias: a, Distance: distance})
		}
	}

	sort.Slice(aliases, func(i, j int) bool { return aliases[i].Distance < aliases[j].Distance })
	return aliases

}

func (aDb *AddressesDB) getAliases(idAddress int) []Alias {
	idx_low := sort.Search(len(aDB.IAddressAlias), func(i int) bool {
		return aDB.Alias[aDB.IAddressAlias[i]].IDAddress >= idAddress
	})

	idx_high := sort.Search(len(aDB.IAddressAlias), func(i int) bool {
		return aDB.Alias[aDB.IAddressAlias[i]].IDAddress > idAddress
	})
	aliases := make([]Alias, 0)

	for i := idx_low; i < idx_high; i++ {
		a := aDb.Alias[aDB.IAddressAlias[i]]
		aliases = append(aliases, a)

	}

	return aliases
}

func (aDB AddressesDB) completeByLevelAlias(level int, partName string) []Alias {
	partName = normalize(partName, aDB.t)
	partName = strings.ToUpper(partName)

	idx_low := sort.Search(len(aDB.ILevelNameAlias), func(i int) bool {
		a := aDB.M[aDB.Alias[aDB.ILevelNameAlias[i]].IDAddress]
		if a.Level < level {
			return false
		} else if a.Level > level {
			return true
		} else {
			n := min(len(partName), len(aDB.Alias[aDB.ILevelNameAlias[i]].Name))
			return aDB.Alias[aDB.ILevelNameAlias[i]].Name[:n] >= partName
		}

	})

	idx_high := sort.Search(len(aDB.ILevelNameAlias), func(i int) bool {
		a := aDB.M[aDB.Alias[aDB.ILevelNameAlias[i]].IDAddress]
		if a.Level < level {
			return false
		} else if a.Level > level {
			return true
		} else {

			n := min(len(partName), len(aDB.Alias[aDB.ILevelNameAlias[i]].Name))
			return aDB.Alias[aDB.ILevelNameAlias[i]].Name[:n] > partName
		}
	})

	aliases := make([]Alias, 0)
	for i := idx_low; i < idx_high; i++ {
		aliases = append(aliases, aDB.Alias[aDB.ILevelNameAlias[i]])
	}
	return aliases
}

func (aDB AddressesDB) completeByParentAlias(idparent int, partName string) []Alias {
	partName = normalize(partName, aDB.t)
	partName = strings.ToUpper(partName)

	idx_low := sort.Search(len(aDB.IParentNameAlias), func(i int) bool {
		a := aDB.M[aDB.Alias[aDB.IParentNameAlias[i]].IDAddress]
		if a.IDParent < idparent {
			return false
		} else if a.IDParent > idparent {
			return true
		} else {
			n := min(len(partName), len(aDB.Alias[aDB.IParentNameAlias[i]].Name))
			return aDB.Alias[aDB.IParentNameAlias[i]].Name[:n] >= partName
		}

	})

	idx_high := sort.Search(len(aDB.IParentNameAlias), func(i int) bool {
		a := aDB.M[aDB.Alias[aDB.IParentNameAlias[i]].IDAddress]
		if a.IDParent < idparent {
			return false
		} else if a.IDParent > idparent {
			return true
		} else {

			n := min(len(partName), len(aDB.Alias[aDB.IParentNameAlias[i]].Name))
			return aDB.Alias[aDB.IParentNameAlias[i]].Name[:n] > partName
		}
	})

	aliases := make([]Alias, 0)
	for i := idx_low; i < idx_high; i++ {
		aliases = append(aliases, aDB.Alias[aDB.IParentNameAlias[i]])
	}
	return aliases
}
