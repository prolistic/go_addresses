package main

import (
	"encoding/json"
	"fmt"
	"log"
	_ "net/http/pprof"
	"os"
	"runtime"
	"runtime/debug"
	"time"
)

type DatabaseConfig struct {
	Server   string
	DB       string
	User     string
	Password string
}

type ServerConfig struct {
	Address string
	Port    int
}
type Configuration struct {
	Database DatabaseConfig
	Server   ServerConfig
}

// @title Recemail Addresses Api
// @version 1.0
// @description Address Database Api
// @termsOfService http://swagger.io/terms/
// @contact.name API Support
// @contact.email frederic.carron@prolistic.ch
// @host localhost:10000
// @BasePath /
func main() {
	file, _ := os.Open("conf.json")
	defer file.Close()
	decoder := json.NewDecoder(file)
	configuration := Configuration{}
	err := decoder.Decode(&configuration)
	if err != nil {
		fmt.Println("error:", err)
		panic("configuration not found")
	}
	start1 := time.Now()
	aDB := makeAddressesDB(configuration.Database)
	elapsed1 := time.Since(start1)
	log.Printf("complete  time load  = %s\n", elapsed1)
	aDB.serialize()
	//aDB := loadAddressesDB()
	//var aDB *AddressesDB
	runtime.GC()
	debug.FreeOSMemory()

	Serve(aDB, configuration.Server)
}
